
var port = process.env.PORT || 8888;
var domain = "http://your-domain.com:" + port;

var util = require('util');
var events = require('events');
var eventEmitter = new events.EventEmitter();

var socket = require( 'socket.io' );
var express = require( 'express' );
var http = require( 'http' );

var memcached = require('memcached');
var mem = new memcached('localhost:11211');

var app = express();
app.use(logErrors);

function logErrors(err, req, res, next) {

  console.log(err.stack);
  next(err);
}

var server = http.createServer( app );
var io = socket.listen( server );

function parse_cookies(_cookies) {
    var cookies = {};

    _cookies && _cookies.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        cookies[ parts[ 0 ].trim() ] = ( parts[ 1 ] || '' ).trim();
    });

    return cookies;
}

io.configure(function (){

	function auth (data, fn){
	
		var cookies = parse_cookies(data.headers.cookie);
		console.log('PHPSESSID: ' + cookies.PHPSESSID);
		
		mem.get('sessions/' + cookies.PHPSESSID, function (err , reply){
		
			fn(null, reply);    
		});
	};
	
	io.set('authorization', auth);
	io.set('log level', 1);
	io.set('origins', 'http://your-domain.com:80 http://www.your-domain.com:80');
});

var maximumTags = 10;

var prototypeArray = require( './prototypes/array.js' );
var prototypeDate = require( './prototypes/date.js' );

var instagram = require('instagram-node').instagram();
var tags = require('./json/tags.js');

instagram.use({
  client_id: 'YOUR_CLIENT_ID',
  client_secret: 'YOUR_CLIENT_SECRET'
});

var renderTimer;
var lastRenderTime = new Date();
var deletingSubscriptions = false;
var deletingSubscription = false;
var addingSubscription = false;
var subscriptionTotal = 0;
var subscriptions = {};

function __status(code,message,tag)
{
	var tag = tag || null;
	return {code:code,message:message,tag:tag};
}

function renderTags(force)
{
	var now = new Date();
	if(Date.msBetween(lastRenderTime,now) > 60 || force)
	{
		lastRenderTime = new Date();
		io.sockets.emit('renderTags',subscriptions);
	}
}

function deleteSubscriptions(){

	if(deletingSubscriptions)
	{
		return __status("DELETING_TAGS","Please wait all hashtags are being deleted.");
	}
	
	deletingSubscriptions = true;
	instagram.del_subscription({ all: true }, function(err, result, limit){
	
		var response = __status("ERROR_TAGS_DELETION","Could not delete all tag subscribtions.");
		if(!err)
		{
			subscriptionTotal = 0;
			subscriptions = {};
			renderTags(true);
			response = __status("TAGS_DELETED","Deleted all subscription.");
		}
		else console.log(err);
		
		io.sockets.emit('status',response);
		deletingSubscriptions = false;
	});
	
	return __status("REQUESTING_TAGS_DELETION","Deleting all subscriptions.");
}

function deleteTagSubscription(tag)
{
	if(deletingSubscription)
	{
		return __status("DELETING_TAG","Please wait a hashtag is being deleted.");
	}

	if(typeof subscriptions[tag] == 'undefined')
	{
		return __status("TAG_NOT_EXISTS","The hashtag does not exists.");
	}
	
	deletingSubscription = true;
	instagram.del_subscription({id:subscriptions[tag].id}, function(err,result,limit){
	
		var response = __status("ERROR_TAG_DELETION","Could not delete the hashtag subscribtion.",tag);
		if(!err)
		{
			subscriptionTotal--;
			delete subscriptions[tag];
			
			renderTags(true);
			response = __status("TAG_DELETED","Deleted hashtag subscription.",tag);
		}
		else console.log(err);
		
		io.sockets.emit('status',response);
		deletingSubscription = false;
		eventEmitter.emit("tagDeleted",tag);
	});
	
	return __status("REQUESTING_TAG_DELETION","Deleting hashtag.",tag);
}

function addSubscription(tag)
{
	if(!tag)
	{
		return __status("TAG_INPUT_EMPTY","The hashtag input is empty.");
	}
	
	if(addingSubscription)
	{
		return __status("ADDING_TAG","Please wait a subscribtion is being verified.");
	}
	
	if(typeof subscriptions[tag] != 'undefined' )
	{
		return __status("TAG_EXISTS","The hashtag already exists.");
	}
	
	if(subscriptionTotal >= maximumTags)
	{
		return __status("TAGS_EXCEEDED","Maximum tag subscriptions exceeded.");
	}
	
	addingSubscription = true;
	instagram.add_tag_subscription(
		tag, 
		(domain + '/callback/' + tag), 
		function(err, result, limit){
		
			var response = __status("ERROR_TAG_SUBSCRIPTION","Could not subscribe the hashtag.",tag);
			if(!err)
			{
				instagram.tag_media_recent(
					tag,
					function(err, medias, pagination, limit){
						
						response = __status("ERROR_TAG_MEDIA","Could not find hashtag media.",tag);
						if(!err)
						{
							var url = "/pub/img/no-image.png";
							if(medias && medias[0] && medias[0].images.standard_resolution.url)
							{
								url = medias[0].images.standard_resolution.url;
							}
						
							subscriptionTotal++;
							subscriptions[tag] = {
							
								id:result.id,
								count:0,
								timestamp:0,
								url:url,
								deleted:false
								
							};
							
							response = __status("TAG_CREATED","Created a hashtag subscription with media.",tag);
							renderTags(true);
						}
						else
						{
							console.log(err);
							instagram.del_subscription({id:result.id}, function(err,result,limit){});
						}
						
						io.sockets.emit('status',response);
						addingSubscription = false;
					});
				
				response = __status("TAG_SUBSCRIBED","Subcribed hashtag.",tag);
			}
			else
			{
				console.log(err);
				addingSubscription = false;
			}
			
			io.sockets.emit('status',response);
		});
		
	return __status("REQUESTING_TAG_SUBSCRIPTION","Subscribing hashtag.",tag);
}

deleteSubscriptions();

io.sockets.on( 'connection', function( client ){
	
	var ip = client.handshake.address;
    console.log( "New Client Connected! ID:" + client.id + ' IP:' + ip.address);
	
	io.sockets.socket(client.id).emit('connected', {test: 'Hello World from Instabubbles!'})
	io.sockets.socket(client.id).emit('renderTags',subscriptions);
	
    client.on( 'disconnect', function( data ){
	
		console.log('Client Disconnected. ID:' + client.id);
    });
	
	client.on( 'addTag', function( data ){
	
		var response = addSubscription(data.tag);
		io.sockets.socket(client.id).emit('status',response);
		
    });
	
	client.on( 'addTagRandom', function( data ){
	
		var tag = tags[Math.floor(Math.random() * tags.length)].title;
		while(typeof subscriptions[tag] != 'undefined')
		{
			tag = tags[Math.floor(Math.random() * tags.length)].title;
		}
	
		var response = addSubscription(tag);
		io.sockets.socket(client.id).emit('status',response);
		
    });
	
	client.on( 'removeTag', function( tagName ){
		
		var response = deleteTagSubscription(tagName);
		io.sockets.socket(client.id).emit('status',response);
	});
	
	client.on( 'removeTagAll', function( data ){
		
		var response = deleteSubscriptions();
		io.sockets.socket(client.id).emit('status',response);
	});
});

app.get('/callback/:id', function(req, res){

	res.end(req.query['hub.challenge']);
});

app.post('/callback/:id', function(req, res){

	var tag = req.params.id;
	if(typeof subscriptions[tag] != 'undefined')
	{
		subscriptions[tag].count++;
		if(subscriptions[tag].count >= 300)
		{
			subscriptions[tag].count = 300;
			subscriptions[tag].deleted = true;
			if(!deletingSubscription)
			{
				var response = deleteTagSubscription(tag);
				io.sockets.emit('status',response);
			}
		}
		
		renderTags();
	}
	
	res.end();
});

eventEmitter.on("tagDeleted",function(tagName){

	for(tag in subscriptions)
	{
		if(subscriptions[tag].deleted)
		{
			var response = deleteTagSubscription(tag);
			io.sockets.emit('status',response);
			break;
		}
	}

});

//-------------------------------------------------------------------------------------------------------------

app.get('/', function(req,res){

	var response = __status("SERVER_ONLINE","Server is running.");
	res.json(response);
});

app.get('/list', function(req,res){

	instagram.subscriptions(function(err, result, limit){

		if(err) return res.json(err);
		res.json(result);
	});

});

app.get('/list2', function(req,res){

	res.json(subscriptions);

});

app.get('/delete', function(req,res){

	var response = deleteSubscriptions();
	res.json(response);
});

server.listen(port);
console.log("Listening on port " + port);