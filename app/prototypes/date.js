
Date.msBetween = function( date1, date2 ){

	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();

	var difference_ms = date2_ms - date1_ms;

	return difference_ms; 
}