<?php 

	require __DIR__ . '/memcachedSessionHandler.class.php';
	
	session_set_cookie_params(0, '/', '.your-domain.com');
	
	session_start();
	$_SESSION['auth'] = 1;

?>
<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<title>Instabubbles</title>
		
		<meta name="keywords" 		content="Instabubbles,Instagram,Bubbles,Images,Tags,Tag,Hashtag,Realtime" />
		<meta name="description" 	content="Instagram tag growth in realtime." />
		
		<meta name="author" 	content="Anders,Ariel,Erika,Josh,Magne" />
		<meta name="generator" 	content="HTML,JavasScript,NodeJS" />
		<meta name="viewport" 	content="width=device-width, initial-scale=1, maximum-scale=1" />
		
		<link rel='icon' type='image/vnd.microsoft.icon' href='favicon.ico' />
		<link rel="stylesheet" href="pub/css/style.css" />
		
		<script src="pub/node_modules/socket.io-client/dist/socket.io.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'GA_ID', 'DOMAIN');
		  ga('send', 'pageview');

		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class='group-0'>
				<canvas id="instabubblesCanvas" width="320" height="480"></canvas>
				<div id="status"></div>
			</div>
			
			<div class='group-1'>
				<header class="logo">
					<h1>Instabubbles</h1>
					<p>Instagram tag growth in realtime.</p>
				</header>
				
				<form id="addTagForm">
					<div>
						<input type="text" name="hash" id="hash" placeholder='Enter hashtag' />
					</div>
					<div>
						<input type="submit" name="submitTag" value="Submit"/> 
						<input type="button" name="submitTagRandom" value="Random" id="addRandomTag" />
					</div>
				</form>
				<span class='icon-move-down'>⇣</span>
			</div>
			
			<div class='group-2'>
				<div class="hashlist">
					<h2>Active Hashtags</h2>
						<div id="instabubbles">
							<ul></ul>
						</div>
				</div>
				<footer id="footer">
					<p>
						Instabubbles 2014 - 
						<a href='https://bitbucket.org/joshgomez/instabubbles'>Source</a>
						<br />
						Anders, 
						Ariel, 
						Erika,
						<a href='http://www.xuniver.se/'>Josh</a> 
						and 
						Magne
					</p>
				</footer>
			</div>
		</div>
		
		<!--
		<script src="pub/js/utilities.js"></script>
		
		<script src="pub/js/audioHandler.js"></script>
		<script src="pub/js/bubbles.js"></script>
		<script src="pub/js/canvas.js"></script>
		
		<script src="pub/js/init.js"></script>
		<script src="pub/js/socket.js"></script>
		<script src="pub/js/main.js"></script>
		-->
		
		<script src="pub/javascripts.php"></script>
	</body>
</html>

