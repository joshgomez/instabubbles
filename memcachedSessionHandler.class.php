<?php

	namespace Instabubbles\Session\SaveHandler;
	
	class MemcachedSessionHandler implements \SessionHandlerInterface
	{
		private $sessionName;
		private $memcached;
		private $lifetime;
		
		public function __construct()
		{
			$this->memcached = new \Memcached();
			$this->memcached->addServer('localhost', 11211);
			
			session_set_save_handler(
				array($this, 'open'),    
				array($this, 'close'),
				array($this, 'read'),
				array($this, 'write'),
				array($this, 'destroy'),
				array($this, 'gc')
			);
			
			register_shutdown_function('session_write_close');
		}
		
		public function close()
		{
			$this->memcached->quit();
			return true;
		}

		public function destroy($id)
		{
			return $this->memcached->delete("sessions/{$id}");
		}

		public function gc($maxlifetime)
		{
			return true;
		}

		public function open($savePath, $name)
		{
			$this->sessionName = $name;
			$this->lifetime = ini_get('session.gc_maxlifetime');
			return true;
		}

		public function read($id)
		{
			$_SESSION = json_decode($this->memcached->get("sessions/{$id}"), true);

			if (isset($_SESSION) && !empty($_SESSION) && $_SESSION != null)
			{
				return session_encode();
			}

			return '';
		}

		public function write($id, $data)
		{
			return $this->memcached->set("sessions/{$id}", json_encode($_SESSION),$this->lifetime);
		}
	}
	
	new MemcachedSessionHandler();
