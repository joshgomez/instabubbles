<?php

	function setCacheHeader($etag,$offset)
	{
		$time = time();
		
		header("Etag: ".$etag);
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s',$time) . ' GMT');
		header('Expires: ' . gmdate('D, d M Y H:i:s', $time + $offset) . ' GMT');
		header('Pragma: cache');
		header('Cache-Control: max-age=' . $offset . ', must-revalidate');
		header('Vary: Accept-Encoding');
	}

	$offset = 3600 * 5;
	$etag = hash_file('md5',$_SERVER['SCRIPT_FILENAME']);

	ob_start('ob_gzhandler');
	
	setCacheHeader($etag,$offset);
	header('Content-Type: application/x-javascript; charset=utf-8');
	
	$files = [
	
		"utilities.js",
		"audioHandler.js",
		"bubbles.js",
		"canvas.js",
		"init.js",
		"socket.js",
		"main.js"
		
	];
	
	foreach($files as $value)
	{
		require __DIR__ ."/js/".$value;
		print "\n";
	}
	
	ob_end_flush();

?>