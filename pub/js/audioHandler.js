function AudioHandler()
{
  this.sounds = {};
}

AudioHandler.prototype.addFolder = function(name, folder, ext)
{
  var extension = this.getExtension();
  var dir = folder;

  _this = this;

  this.sounds[name] = [];
  
  $.ajax({
    url: dir,
    success: function (data) {
        $(data).find("a:contains(" + extension + ")").each(function () {
            var path = folder + "/" + /[^/]*$/.exec(this.href)[0];

            var audio = new Audio(); 
            audio.src = path;
            _this.sounds[name].push(audio);
        });
    }
  });
}

AudioHandler.prototype.getExtension = function()
{
  var audio = new Audio();
  
  if (audio.canPlayType('audio/ogg;')) 
	return "ogg";
  
  return "mp3";
}

AudioHandler.prototype.playRandom = function(name)
{
  var rand = Math.floor(Math.random() * this.sounds[name].length);
  this.sounds[name][rand].play();
}