
  function Bubble(canvas, xPos, yPos, tagName)
  {
    this.canvas = canvas;
   
	this.radius = this.canvas.initialSize;
    this.color = 'rgba(0, 0, 255, .1)';
    this.lineWidth = 2;
    this.strokeColor = 'rgba(255, 255, 255, .3)';
    this.x = xPos;
    this.y = yPos;
	
	this.tag = tagName;
	this.highlighted = false;
	
	this.img;
	this.imgLoaded = false;
	
	this.minSpeed = 0.3;
	this.maxSpeed = 0.8;
	this.speed = this.getRandSpeed(this.minSpeed, this.maxSpeed);
	
	this.minDir = 0;
	this.maxDir = 359;
	this.direction = this.getRandDir(this.minDir, this.maxDir);
	
	this.loadImage();
	this.lastBounce = "X";
  }

  Bubble.prototype.getRandSpeed = function(min, max)
  {
  	return Math.random() * (max - min) + min;
  }

   Bubble.prototype.getRandDir = function(min, max)
  {
  	return Math.floor(Math.random() * (max - min) + min);
  }

  Bubble.prototype.getRadiansFromDegrees = function(degrees)
  {
  	return degrees * (Math.PI/180);
  }

  Bubble.prototype.move = function()
  {
  	var x2 = Math.cos(this.getRadiansFromDegrees(this.direction));
  	var y2 = Math.sin(this.getRadiansFromDegrees(this.direction));
  	
	this.x += x2 * this.speed;
  	this.y += y2 * this.speed;
  }

  Bubble.prototype.checkCanvasCollision = function()
  {
	var rad = this.radius;
	
	if(this.y > this.canvas.height - rad) 
	{
		this.y = this.canvas.height - (rad + 1);
		if(this.lastBounce !== "D")
		{
			//Down
			this.direction = -this.direction;
			this.lastBounce = "D";
		}
	}
	else if(this.y < rad)
	{
		this.y = rad + 1;
		if(this.lastBounce !== "U")
		{
			//Up
			this.direction = -this.direction;
			this.lastBounce = "U";
		}
	}
	else if(this.x < rad) 
	{
		this.x = rad + 1;
		if(this.lastBounce !== "L")
		{
			//Left
			this.direction = 180-this.direction;
			this.lastBounce = "L";
		}
	}
	else if(this.x > this.canvas.width - rad)
	{
		this.x = this.canvas.width - (rad + 1);
		if(this.lastBounce !== "R")
		{
			//Right
			this.direction = 180-this.direction;
			this.lastBounce = "R";
		}
	}
  }
  

  Bubble.prototype.draw = function()
  {
	var c = this.canvas.context;
	if(!this.canvas.bubbleData[this.tag].deleted)
	{
		if(this.highlighted)
			this.highlight();
		
		
		if(this.imgLoaded)
		{
			this.scaleImage();
			
			c.save();
			c.beginPath();
			
			c.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
			
			c.clip();
			c.globalAlpha = 0.6;
			c.drawImage(this.img, this.x-this.radius,this.y-this.radius, this.radius*2, this.radius*2);
			
			c.closePath();
		}
		
		c.strokeStyle = 'rgba(215,215,255,1)';
		c.lineWidth = 4;
		c.stroke();
		
		c.beginPath();
		c.globalAlpha = 1;
		c.drawImage(this.canvas.bubbleImg, this.x-this.radius,this.y-this.radius, this.radius*2, this.radius*2);
		c.closePath();
		
		c.restore();
		
		c.fillStyle = "white";
		c.font = "bold "+this.setFontSize()+"px verdana";
		c.textAlign = "center";
		c.fillText("#"+this.tag, this.x, this.y, this.radius * 2);
	}
  }
  
  Bubble.prototype.loadImage = function()
  {
	this.img = new Image();
	this.img.src = this.canvas.bubbleData[this.tag].url;
	var _this = this;
	this.img.onload = function(){
		
		_this.imgLoaded = true;
	};
  }
  
  Bubble.prototype.scaleImage = function()
  {
	this.img.width = this.radius * 2;
	this.img.height = this.radius * 2;
  }

  Bubble.prototype.setFontSize = function()
  {
	var size;
	if((this.radius / 7) < 12)
		return 12;
		
	return parseInt(this.radius / 7);
  }
  
  Bubble.prototype.highlight = function()
  {
		var context = this.canvas.context;
	
		context.beginPath();
		context.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
		context.fillStyle = "rgba(255, 255, 255, 1)";
		context.fill();
		context.closePath();
  }
  
  Bubble.prototype.grow = function()
  {
	var newRadius = this.canvas.initialSize + canvas.bubbleData[this.tag].count;
	var ratio = this.getSizeRatio();
	
	this.radius = Math.floor(newRadius * ratio);
  }
  
  Bubble.prototype.getSizeRatio = function()
  {
	var w = this.canvas.width;
	var ratio = this.canvas.width / 1024;
	
	if(ratio >= 1)
		return 1;
	
	return ratio;
  }  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  