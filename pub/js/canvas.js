 
  function Canvas(canvasEl)
  {
    var canvas   = document.getElementById(canvasEl);
	
	canvas.width = $(window).width();
	canvas.height = $(window).height();
	
	this.canvasEl   = canvas;
    this.context = canvas.getContext('2d');
    this.bubbles = {};
    this.height  = canvas.height;
    this.width   = canvas.width;
    this.lastTime = 0;
	
	this.bubbleData;
	
	this.bubbleImg = new Image();
	this.bubbleImg.src = "pub/img/bubble_overlay_blue2.png";
	
	this.audio = new AudioHandler();
	this.audio.addFolder("pops", "pub/fx/pops");
	this.audio.addFolder("plings", "pub/fx/plings");
	
	this.initialSize = 30;
	this.maxSize = 300;
  }
  
  Canvas.prototype.getRandPos = function(max)
  {
    var min = this.initialSize;
    return Math.floor(Math.random() * ((max-this.initialSize) - min + 1)) + min;
  }

  Canvas.prototype.init = function()
  {
	this.tick();
  }
  
  Canvas.prototype.tryClick = function(x,y)
  {
	for(index in this.bubbles)
	{
		var col = this.collision(x, y, 1, this.bubbles[index].x, this.bubbles[index].y, this.bubbles[index].radius);
		
		if(col)
		{
			this.removeBubble(index);
			break;
		}
	}
	
  }
  
  Canvas.prototype.addBubble = function(tagName)
  {
	do
    {
      var x = this.getRandPos(this.width);
      var y = this.getRandPos(this.height);

      for(index in  this.bubbles)
      {
        var collision = this.collision(x, y, this.initialSize, this.bubbles[index].x, this.bubbles[index].y, this.bubbles[index].radius);
		
        if(collision)
          break;
      }

    } while(collision)
  
	this.bubbles[tagName] = new Bubble(this, x,y, tagName);
	this.audio.playRandom("plings");
  }

  Canvas.prototype.collision = function(p1x, p1y, r1, p2x, p2y, r2) 
  {
    var a;
    var x;
    var y;

    a = r1 + r2;
    x = p1x - p2x;
    y = p1y - p2y;

    if ( a > Math.sqrt( (x*x) + (y*y) ) ) 
    {
      return true;
    } 
    else 
    {
      return false;
    }   
  }

  Canvas.prototype.popBubble = function()
  {
    this.audio.playRandom("pops");
  }

  Canvas.prototype.removeBubble = function(tagName)
  {
	socket.emit("removeTag", tagName);
  }
  
  Canvas.prototype.tick = function()
  {
    var now = Date.now();
    if(this.lastTime != 0)
    {
      var dt = (now - this.lastTime) / 1000.0;
      if(dt)
      {
        this.clear();
        for(index in this.bubbleData)
        {
			if(this.bubbles[index])
			{
				this.bubbles[index].grow();
				this.bubbles[index].move();
				this.bubbles[index].checkCanvasCollision();
				
				this.bubbles[index].draw();
			}
        }
      }
    }
    
    this.lastTime = now;
    window.requestAnimFrame(this.tick.bind(this));
  }
  
  Canvas.prototype.resetBounce = function()
  {
	for(index in this.bubbles)
	{
		this.bubbles[index].lastBounce = "X";
	}
  }

  Canvas.prototype.clear = function()
  {
    this.context.clearRect(0, 0, this.width,this.height);
    this.context.beginPath();
    this.context.rect(0, 0, this.width,this.height);
    this.context.closePath();
  }