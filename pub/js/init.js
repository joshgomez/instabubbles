
window.requestAnimFrame = (function()
  {
    return  window.requestAnimationFrame  ||
      window.webkitRequestAnimationFrame  ||
      window.mozRequestAnimationFrame     ||
      function( callback ){ window.setTimeout(callback, 1000 / 60); };
    
  })();

var socket = io.connect("your-domain.com:8888");
var canvas = new Canvas("instabubblesCanvas");

canvas.init();
