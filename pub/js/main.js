
(function(){

	var timerAddTag;

	$('#instabubbles ul').on("click","li a",function(e){

		e.preventDefault();
		var tag = e.target.parentNode.getAttribute("data-id");
		canvas.removeBubble(tag);
	});

	$("#addTagForm").submit(function(e){
		e.preventDefault();
		
		if(timerAddTag)
			clearTimeout(timerAddTag);
		
		timerAddTag = setTimeout(function(){
		
			socket.emit("addTag", { tag:  e.target.hash.value} );
			e.target.reset();
		},100);
	});

	$("#addRandomTag").click(function(e){
	
		e.preventDefault();
		
		if(timerAddTag)
			clearTimeout(timerAddTag);

		timerAddTag = setTimeout(function(){
		
			socket.emit("addTagRandom", {} );
		},100);
	});

	$("#removeTagAll").click(function(e){
		e.preventDefault();
		socket.emit("removeTagAll", {} );
	});
	
 	$(".icon-move-down").click(function(e){
		e.preventDefault();
		
	   var height = $(document).height();
		if($(window).scrollTop() + $(window).height() != height)
		{
			$('html, body').animate({ scrollTop: height }, 500);
		}
	});
	
	$(document).click(function(e){
	
		var elName = e.target.nodeName;
		if(elName != "INPUT" && elName != "A")
			canvas.tryClick(e.clientX, e.clientY);
	});
	
	$("#instabubbles").on("mouseover", "li", function(e) {
		if(e.target.nodeName === "LI")
		{
			var tagName = $(e.target).attr("data-id");
			canvas.bubbles[tagName].highlighted = true;
		}
			
	});

	$("#instabubbles").on("mouseout onmousemove blur", "li", function(e) {

		for(index in canvas.bubbles)
		{
			canvas.bubbles[index].highlighted = false;
		}
		
	});

	$( window ).resize(function(){
		canvas.width = $(window).width();
		canvas.canvasEl.width = canvas.width;
		canvas.height = $(window).height();
		canvas.canvasEl.height = canvas.height;
		canvas.resetBounce();
	});
	
})();