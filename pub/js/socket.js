
socket.on('status', function(data){
	
	console.log(data);
	
	switch(data.code)
	{
		case "TAG_CREATED":
			__statusFadeOut(data.tag + "!");
			break;
		case "TAG_DELETED":
			$('#status').html("");
			delete canvas.bubbles[data.tag];
			canvas.popBubble();
			__statusFadeOut("Plop!");
			break;
		case "TAGS_DELETED":
			__statusFadeOut("Plop!");
			break;
		case "TAG_SUBSCRIBED":
			__loading("Blowing bubble...");
			break;
		case "REQUESTING_TAG_SUBSCRIPTION":
			
			var randomText = [
			
				"Fetching soap...",
				"Mixing soap water...",
				"Calculating bubble vectors...",
				"Connecting soap atoms..."
				
			];
			
			__loading(randomText[Math.floor(Math.random() * randomText.length)]);
			break;
		case "REQUESTING_TAGS_DELETION":
		case "REQUESTING_TAG_DELETION":
			__loading("Deploying needle...");
			break;
		case "ADDING_TAG":
			__loading(data.message);
			break;
		case "ERROR_TAG_MEDIA":
		case "ERROR_TAG_SUBSCRIPTION":
			__statusFadeOut("Bubble popped! :(",1000);
			break;
		case "TAG_INPUT_EMPTY":
		case "TAG_EXISTS":
		case "TAGS_EXCEEDED":
		case "DELETING_TAGS":
		case "DELETING_TAG":
			__statusFadeOut(data.message,1000);
			break;
		default:
			__statusFadeOut("");
	}
});

socket.on( 'connected', function( data ){

	console.log(data);
});


socket.on( 'renderTags', function( data ){

	var textHTML = "";
	for(index in data)
	{
		textHTML += '<li data-id="'+index+'">#'+index + ': ' + data[index].count + ' <a href="#">x</a></li>';
	}
	
	canvas.bubbleData = data;
	for(index in data)
	{
		if(typeof canvas.bubbles[index] == "undefined")
		{
			canvas.addBubble(index);
		}
	}
	
	$('#instabubbles ul').html(textHTML);

});