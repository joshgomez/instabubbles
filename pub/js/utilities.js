
function __loading(text)
{
	var $status = $('#status');
	$status.hide();

	var htmlText = "";
	htmlText += "<img src='pub/img/loading.gif' / ><br />" + text;
	$status.html(htmlText).fadeIn();
}

function __statusFadeOut(text,time)
{
	var time = time || 100;
	var $status = $('#status');
	
	$status.show();
	$status.html(text).delay(time).fadeOut();
}